# alta3research-gitlab



## Getting started

1. [x] Create a public project on GitLab called, alta3research-gitlab
2. [x] Your project should contain a Wiki with at least three (3) pages. What those pages display is up to you, but all the pages should link to each other and be constructed
using Markdown.
3. [x] Your repository should contain, .gitlab-ci.yml.
4. [x] The main branch should have a README.md, .gitlab-ci.yml, and main.py (any other code is fine, it doesn't need to be Python).
5. [x] README.md should be provisioned with some details about your project.
6. [x] .gitlab-ci.yml pipeline needs at least one stage. The job it executes may be benign, but the pipeline should run successfully.
7. [x] In addition to main, your repository should have the branch feature-fix. On this branch, made some edits to one of your files (README.md) is fine, and then start a merge.
8. [x] Add the author, @rzfeeser, as a collaborator to your project.
9. [x] Create a Webhook. The URL target should be http://enterpriseapp.example.com. You can create your own secret token.
10. [x] Create two (2) milestones. The details are up to you.
11. [x] Create three (3) issues on your project. The details are up to you. Be sure to apply Milestones to the issues, along with labels.
12. [x] When you finish, email to info@alta3.com containing the following:
13. [x] Subject: - Alta3 Research Git and GitLab Certification - Grading Request
  * In the body be sure to include:
    * Name: - What you'd like printed on the certification
    * Email: - Email address you used to enroll in the course
    * GitLab URL: - The URL of your public GitLab repository containing your work to be graded
    * Course Start: - Date your course started on
    * Instructor: - Name of your instructor (if you remember)
